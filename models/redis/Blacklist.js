/**
 * Blacklist.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let RedisService = require('../../config/redisDatasource');

function setBlacklist(list) {
	RedisService.setToRedis("blacklist",list,-1);
}

function getBlacklist() {
	return new Promise((resolve, reject) => {
		RedisService.getFromRedis("blacklist", (err, result) => {
			if (err) {
				resolve({});
			} else {
				resolve(result);
			}
		});
	});
}

module.exports = {
	setBlacklist: setBlacklist,
	getBlacklist: getBlacklist
};