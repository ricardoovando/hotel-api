/**
 * Booking.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let RedisService = require('../../config/redisDatasource');

function setBooking(bookingId, booking) {
	RedisService.setToRedis("bookings:" + bookingId, booking, 120);
}

function getBooking(bookingId) {
	return new Promise((resolve, reject) => {
		RedisService.getFromRedis("bookings:" + bookingId, (err, result) => {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
}

function existsBooking(bookingId) {
	return new Promise((resolve, reject) => {
		RedisService.existInRedis("bookings:" + bookingId, (err, result) => {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
}

module.exports = {
	setBooking: setBooking,
	getBooking: getBooking,
	existsBooking: existsBooking
};