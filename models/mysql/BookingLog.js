/**
 * BookingLog.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let MysqlService = require('../../config/mysqlDatasource');

const bookinglog = MysqlService.define({
  name: 'BookingLog',
  options: {
    mysql: {
      table: 'SEARCH_LOG'
    }
  },
  attributes: {
    id: {
      type: Number,
      id: true,
      length: 11,
      //required: true, // Es auto incrementable en la DB
      mysql: {
        columnName: 'ID_SEARCH_LOG',
        dataType: 'int',
        dataLength: 11,
        nullable: false
      }
    },
    request: { 
      type: String,
      required: false,
      mysql: {
        columnName: 'REQUEST',
        dataType: 'longtext',
        nullable: true
      }
    },
    response: { 
      type: String,
      required: true,
      mysql: {
        columnName: 'RESPONSE',
        dataType: 'longtext',
        nullable: false
      }
    },
    currentDate: {
      type: Date,
      required: false,
      mysql: {
        columnName: 'CURR_DATE',
        dataType: 'datetime',
        nullable: true
      }
    },
    spnr: {
      type: String,
      length: 45,
      required: false,
      mysql: {
        columnName: 'SPNR',
        dataType: 'varchar',
        dataLength: 45,
        nullable: true
      }
    },
    step: {
      type: String,
      length: 100,
      required: false,
      mysql: {
        columnName: 'STEP',
        dataType: 'varchar',
        dataLength: 100,
        nullable: true
      }
    },
    provider: {
      type: String,
      length: 45,
      required: false,
      mysql: {
        columnName: 'PROVIDER',
        dataType: 'varchar',
        dataLength: 45,
        nullable: true
      }
    }
  }
});

module.exports = bookinglog;