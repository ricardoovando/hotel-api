'use strict';
/* jshint strict: false, esversion: 6 */

module.exports = {
	'/itinerary/send/:spnr': {
		method: 'GET',
		controller: 'MailingController',
		action: 'sendItinerary',
		auth: null
	},
	'/itinerary/send/:spnr/:email': {
		method: 'GET',
		controller: 'MailingController',
		action: 'sendItinerary',
		auth: null
	},
	'/itinerary/status/:id': {
		method: 'GET',
		controller: 'MailingController',
		action: 'getSentInfo',
		auth: null
	},
	'/itinerary/pdf/:spnr': {
		method: 'GET',
		controller: 'MailingController',
		action: 'getPdfVoucher',
		auth: null
	}
};