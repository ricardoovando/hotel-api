
'use strict';
/* jshint strict: false, esversion: 6 */

let externalServices = require('cocha-external-services');

let hotelServices 	 = externalServices.hotelServices;
let sendMailService  = externalServices.sendMail;
let mailParser 		 = require('../services/MailingParser');

async function sendItinerary(ctx) {
	ctx.params.channel = 'B2B';
	ctx.params.country = 'CL';
	var email = ctx.params.email || null;
	let itinerary = await new Promise((resolve, reject) => {
		hotelServices.itinerary(ctx.params, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject({
					message: {
						msg: err.msg,
						code: err.name
					}
				});
			} else {
				resolve(result);
			}
		}, ctx.authSession);
	});

	
	var config = await mailParser.parseConfirmationFlightHotel(itinerary, email);
	ctx.body = await new Promise((resolve, reject) => {
		sendMailService.sendMail(config, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
};


async function getPdfVoucher(ctx) {
	ctx.params.channel = 'B2B';
	ctx.params.country = 'CL';

	let itinerary = await new Promise((resolve, reject) => {
		hotelServices.itinerary(ctx.params, (err, result) => {
			if (err) {
				reject({
					message: {
						msg: err.msg,
						code: err.name
					}
				});
			} else {
				resolve(result);
			}
		}, ctx.authSession);
	});

	var pdfStream = await mailParser.parsePdfVoucher(itinerary);

	ctx.body = pdfStream;
	ctx.response.set('Content-Type', 'application/pdf; charset=utf-8');

	if(ctx.params.show){
		ctx.disposition = 'inline; filename=' + ctx.params.spnr + '.pdf';
	} else {
		ctx.attachment(ctx.params.spnr + '.pdf');
	}
	return true;
};

async function getSentInfo(ctx) {
	ctx.status = 200;
	ctx.body = {
		msg: await new Promise((resolve, reject) => {
			sendMailService.getInfoSentByID(this.params.id, function (err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		})
	};
}

module.exports = {
	sendItinerary: sendItinerary,
	getPdfVoucher: getPdfVoucher,
	getSentInfo: getSentInfo
};
