/**
 * PdfGeneratorService.js
 *
 */
var fileSystem = require('fs');

var htmlToPdf = require("phantom-html-to-pdf")({
	//strategy: "dedicated-process",
	//timeout: 15000,
	phantomPath: require("phantomjs-prebuilt").path
});
var logger = require('@cocha/cocha-logger-services')('pdf-generator');
var OPTIONS = { // For details -> https://www.npmjs.com/package/phantom-html-to-pdf
	paperSize: {
		format: 'A4',
		orientation: 'portrait',
		margin: '1cm'
	},
	settings: { dpi: 72 },
	fitToPage: true,
	format: {
		quality: 100
	},
	html: "",
	waitForJS: true,
	allowLocalFilesAccess: true
};

async function generatePdfFromStringToBuffer(_stringHtml) {

	var startTime = Date.now();
	var logObj = {
		function: "generatePdfFromStringToBuffer"
		, params: _stringHtml
		, success: true
		, level: 'info'
	};

	var options = OPTIONS;
	var callback;

	options.html = _stringHtml;

	let pdfGenerated = await new Promise((resolve, reject) => {
		htmlToPdf(options, function (err, pdf) {
			if (err) {
				logObj.success = false;
				logObj.msg = err.msg;
				logObj.level = 'error';
				logObj.responseTime = Date.now() - startTime;
				logger.log(logObj.level, logObj);

				reject({
					message: {
						msg: err.msg,
						code: err.name
					}
				});
			} else {
				logObj.level = 'info';
				logObj.responseTime = Date.now() - startTime;
				logObj.msg = pdf.stream;
				logger.log(logObj.level, logObj);

				var buffer;
				pdf.stream.data = [];
				pdf.stream.on('data', function (chunk) {
					this.data.push(chunk);
				});
				pdf.stream.on('end', function (chunk) {
					buffer = Buffer.concat(this.data);
					resolve(buffer.toString('base64'));
					htmlToPdf.kill();
				});
			}
		});
	});
	return pdfGenerated;
};

async function generatePdfFromStringToStream(_stringHtml, _options, _callback) {
	var startTime = Date.now();
	var logObj = {
		function: "generatePdfFromStringToStream"
		, params: _stringHtml
		, success: true
		, level: 'info'
	};

	var options = OPTIONS;
	var callback;

	options.html = _stringHtml;

	let pdfGenerated = await new Promise((resolve, reject) => {
		htmlToPdf(options, function (err, pdf) {
			if (err) {
				logObj.success = false;
				logObj.msg = err.msg;
				logObj.level = 'error';
				logObj.responseTime = Date.now() - startTime;
				logger.log(logObj.level, logObj);

				reject({
					message: {
						msg: err.msg,
						code: err.name
					}
				});
			} else {
				logObj.level = 'info';
				logObj.responseTime = Date.now() - startTime;
				logObj.msg = pdf.stream;
				logger.log(logObj.level, logObj);

				resolve(pdf.stream)
			}
		});
	});
	return pdfGenerated;
};

async function generatePdfFromUrlToStream(_url) {
	var startTime = Date.now();
	var logObj = {
		function: "generatePdfFromStringToStream"
		, params: _url
		, success: true
		, level: 'info'
	};

	let options = OPTIONS;
	options.url = _url;
	//options.printDelay = 5000;
	options.waitForJS = true; //Specify when the pdf printing starts (window.PHANTOM_HTML_TO_PDF_READY = true; //this will start the pdf printing)
	let pdfGenerated = await new Promise((resolve, reject) => {
		htmlToPdf(options, function (err, pdf) {
			if (err) {
				var error = {
					Message: "Error generating pdf",
					Cause: err
				};
				reject(error);
			} else {
				var buffer;
				pdf.stream.data = [];
				pdf.stream.on('data', function (chunk) {
					this.data.push(chunk);
				});
				pdf.stream.on('end', function (chunk) {
					buffer = Buffer.concat(this.data);
					resolve(buffer);
					htmlToPdf.kill();
				});
			}
		});
	});
	
	return pdfGenerated;
}

module.exports = {
	generatePdfFromStringToBuffer: generatePdfFromStringToBuffer,
	generatePdfFromStringToStream: generatePdfFromStringToStream,
	generatePdfFromUrlToStream: generatePdfFromUrlToStream
};