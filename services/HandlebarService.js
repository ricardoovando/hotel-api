/**
    MailingParser.js

jshint strict: false, esversion: 6 */
"use strict";

// Category image 
function hotelCatImg(_starRating) {
    return getHotelRatingImageTag(_starRating);
}

function headerImage(input){
	return (input==='BCD') ? 'https://www.cocha.com/images/BCD.png':'https://gallery.mailchimp.com/cf8e3f784ee65fee4bea0e12f/images/40b6f0e8-9e6d-4753-a368-559d9b6c99ba.png';
}

function getHotelRatingImageTag(_starRating) {
    _starRating = parseInt(_starRating) || 0;
    const galleryBaseUrl = 'https://gallery.mailchimp.com/cf8e3f784ee65fee4bea0e12f/images/';
    const images = [
        'afa46f93-1ed9-4030-8254-441e4c1ef644.png', // I'm going to set the one start when rating == 0
        'afa46f93-1ed9-4030-8254-441e4c1ef644.png',
        '32d372e0-a063-479d-a042-a3a0b1ed882b.png',
        '3856a52c-be80-4e4d-b2e4-d3dfe5a2f6a0.png',
        'b719cc0c-f057-41ca-8240-5e9852233ddd.png',
        'c3ab6644-08e5-48f4-9551-02e8aa84ab8f.png',
    ];
    return `<img src="${galleryBaseUrl}${images[_starRating]}">`;
}


function capitalize(input) {
    if (!_.isString(input)) {
        return input;
    }
    let output = input.substring(0, 1).toUpperCase() + input.substring(1);
    return output.replace(/ *\([^)]*\) */g, "");
}

function upper(input) {
    if (!_.isString(input)) {
        return input;
    }
    let output = input.toUpperCase();
    return output.replace(/ *\([^)]*\) */g, "");
}


function priceFilter(val,currency){
	let currencySymbol;
    if (currency === 'USD') {
        currencySymbol = 'US$';
    }
    else if (currency === 'CLP') {
        currencySymbol = '$';
    }
    else {
        currencySymbol = currency;
    }
    if (!isNaN(val)) {
        return currencySymbol + Math.ceil(val).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&.');
    }
    else {
        return val;
    }
}

function addHelpers(_handlebars){
	_handlebars.registerHelper('priceFilter',priceFilter);
	_handlebars.registerHelper('upper',upper);
	_handlebars.registerHelper('capitalize',capitalize);
	_handlebars.registerHelper('hotelCatImg',hotelCatImg);
	_handlebars.registerHelper('headerImage',headerImage);	
}

module.exports = {
    addHelpers: addHelpers
}