/**
    MailingParser.js

jshint strict: false, esversion: 6 */
"use strict";

var handlebars = require('handlebars');
var moment = require('moment');
moment.locale('es');
var handlebarsService = require('./HandlebarService');

var fs = require('fs');

var logger = require('@cocha/cocha-logger-services')('email-parser');
var pdfGeneratorService = require('./PdfGeneratorService');
var sendMailService = require('cocha-external-services').sendMail;
const ENV = process.env.NODE_ENV || 'production';

function convertUSDtoCLP(valueExchange, amountExchange) {
    if (amountExchange > 0) {
        return Math.floor(amountExchange * valueExchange)
    } else return 0;
}

async function parsePdfVoucher(_params) {
    let logObj = {
        function: "parsePdfVoucherFlightHotel"
        ,params: _params
        ,success: true
        ,level: 'info'
    };

    let global_merge_vars = await parseGlobalMergeVars(_params);
    let templateVoucherScript = fs.readFileSync('views/templates/hotel.handlebars', 'utf8');

    handlebarsService.addHelpers(handlebars);

    var templateVoucherHtml = handlebars.compile("{{#content}}" + templateVoucherScript + "{{/content}}");
    var compiledVoucherHtml = templateVoucherHtml(global_merge_vars);

    var pdfStream = await pdfGeneratorService.generatePdfFromStringToStream(compiledVoucherHtml);

    return pdfStream;
}

async function parseGlobalMergeVars(_params) {
    var logObj = {
        function: "parseGlobalMergeVars",
        params: _params,
        success: true,
        level: 'info'
    };

    var _config_hotelConfirmation = _params.passengers;
    var _config_hotelNotes = _params.amenities || [];

    //rooms
    var config_rooms = [];
    _.each(_config_hotelConfirmation, function (_config_hotelConfirmation, i) {
        config_rooms.push({
            id: i + 1,
            confirmationNumber: _config_hotelConfirmation.numConfirmacion,
            adultCount: _config_hotelConfirmation.adultsNum,
            childCount: _config_hotelConfirmation.childNum,
            roomType: _config_hotelConfirmation.roomDescription,
            nights: _params.numNights,
            reservationGuest: [
                {
                    firstName: _config_hotelConfirmation.name,
                    lastName: _config_hotelConfirmation.surname,
                    rut: _config_hotelConfirmation.rut
                }
            ],
        });
    });

    //rooms
    var amountHotel = _params.amountUSD;

    //notas del hotel
    var config_hotel_notes = [];
    _.each(_config_hotelNotes.valueAdd, function (_config_hotelNotes) {
        config_hotel_notes += _config_hotelNotes.text + ". ";
    });

    //Google Schemas
    var _googleSchemaHotel = parseGoogleSchemaHotel(_params);

    let checkIn = moment(_params.arrDate, 'YYYY-MM-DD').format("dddd DD MMMM");
    let checkOut = moment(_params.depDate, 'YYYY-MM-DD').format("dddd DD MMMM");

    let paymentInfo;

    let flagAssistance = false;
    let flagTransfer = false;
    let flagService = false;

    if (_.has(_params, 'amountCLP')) {
        if (!_params.hotelDetailItinDTO ||  Object.keys(_params.hotelDetailItinDTO).length === 0) {
            logObj.success = false;
            logObj.recipient_to = _params.paymentPassenger.email;

            if (!_params.numReserva){
                logObj.message = 'Error during parsing [parseConfirmationFlightHotel]: Missing hotel confirmation info data';
                var msg = 'Falta información de confirmación del hotel';
            } else if (!_params.hotelDetailItinDTO) {
                logObj.message = 'Error during parsing [parseConfirmationFlightHotel]: Missing booking hotel detail info data';
                var msg = 'Falta información de confirmación del detalle del hotel';
            } else if (!_params.amountUSD) {
                logObj.message = 'Error during parsing [parseConfirmationFlightHotel]: Missing booking pricing data';
                var msg = 'Falta información de confirmación del pago';
            } 

            logger.log('error', logObj);
            let alerted = await sendAlertEmailNotSent(_params, msg);
            throw {
                status: 503,
                message: { message: msg, alerted: alerted, contact_mail: _params.paymentPassenger.email },
                data: JSON.stringify(_params)
            };
        }
        let totalFee = "0";
        paymentInfo = [{
            fullName: _params.paymentPassenger.name + ' ' + _params.paymentPassenger.surname,
            rut: _params.paymentPassenger.rut,
            telephone: _params.hotelPhone,
            email: _params.paymentPassenger.email,
            amounts: [
                {
                    hotelUSD: _params.amountUSD / (parseFloat((_params.numAdults || "1")) + parseFloat((_params.numChild || "0"))),
                    tasasCargosUSD: totalFee,
                    transferUSD: "0",
                    asistenciaUSD: "0",
                    serviceFeeUSD: "0",
                    totalUSD: parseFloat(_params.amountUSD),
                    totalCLP: _params.amountCLP,
                    hotelCLP: _params.amountCLP / (parseFloat((_params.numAdults || "1")) + parseFloat((_params.numChild || "0"))),
                    tasasCargosCLP: "0",
                    transferCLP: "0",
                    asistenciaCLP: "0",
                    serviceFeeCLP: "0",
                    localTax: "0",
                    totalCLPLocal: _params.amountCLP
                }
            ],
        }];
        flagAssistance = false;
        flagTransfer = false;
        flagService = false;
    }
    ////parse vars
    var global_merge_vars = {
        name: "contexto",
        content: [
            {
                codReservations: [
                    {
                        codReservaHot: _params.numReserva,
                        codReservaVou: _params.paymentPassenger.numConfirmacion,
                        codReservaInt: _params.codInterno,
                        numNegocio: _params.businessNumber,
                        corporationId:'COCHA'
                    }
                ],
                customerInfo: [
                    {
                        email: _params.paymentPassenger.email,
                        firstName: _params.paymentPassenger.name,
                        lastName: _params.paymentPassenger.surname,
                        rut: _params.paymentPassenger.rut,
                        homePhone: _params.paymentPassenger.phone,
                        workPhone: _params.paymentPassenger.phone,
                        extension: _params.paymentPassenger.phone,
                        faxPhone: _params.paymentPassenger.phone,
                        customerAddresses: [
                            {
                                address1: _params.hotelDetailItinDTO.address,
                                city: _params.hotelDetailItinDTO.city || '',
                                stateProvinceCode: _params.hotelDetailItinDTO.stateProvinceCode || '',
                                countryCode: _params.hotelDetailItinDTO.country,
                                postalCode: _params.hotelDetailItinDTO.zipCode,
                                isPrimary: true,
                                type: '',
                            }
                        ]
                    }
                ],
                paymentInfo: paymentInfo,
                itineraryHotel: [
                    {
                        contactFullName: _params.paymentPassenger.name + ' ' + _params.paymentPassenger.surname,
                        confirmationNumber: _params.numReserva,
                        checkInDate: _params.arrDate,
                        checkoutDate: _params.depDate,
                        checkInLongDate: (_params.hotelDetailItinDTO.checkInTime.toLowerCase() === "noon") ? checkIn + ", desde las 12 AM" : (_params.hotelDetailItinDTO.checkInTime === "") ? checkIn : checkIn + ", desde las " + _params.hotelDetailItinDTO.checkInTime,
                        checkOutLongDate: (_params.hotelDetailItinDTO.checkOutTime.toLowerCase() === "noon") ? checkOut + ", hasta las 12 AM" : (_params.hotelDetailItinDTO.checkOutTime === "") ? checkOut : checkOut + ", hasta las " + _params.hotelDetailItinDTO.checkOutTime,
                        adultCount: _params.numAdults,
                        childCount: _params.numChild,
                        nights: _params.numNights,
                        hotelId: _params.hotelDetailItinDTO.id,
                        name: _params.hotelDetailItinDTO.name,
                        address: _params.hotelDetailItinDTO.address,
                        telephone: _params.hotelPhone,
                        cityName: (_params.hotelPhone ? _params.hotelDetailItinDTO.address.concat(". <br>Tel. ", _params.hotelPhone) : _params.hotelDetailItinDTO.address),
                        //cityCode			: _params.hotel.details.
                        zipCode: _params.hotelDetailItinDTO.zipCode,
                        countryCode: _params.hotelDetailItinDTO.country,
                        hotelRating: _params.hotelDetailItinDTO.starRating,
                        mandatoryFeeDescription: _params.hotelDetailItinDTO.mandatoryFeeDescription,
                        feeDescription: _params.hotelDetailItinDTO.feeDescription,
                        policyDescription: _params.hotelDetailItinDTO.policyDescription,
                        cancellationPolicy: _params.cancelPolicy,
                        images: _params.hotelDetailItinDTO.urlImages,
                        notes: config_hotel_notes,
                        rooms: config_rooms,
                    }
                ],
                flagAssistance: flagAssistance,
                flagTransfer: flagTransfer,
                flagService: flagService,
                googleSchemaHotel: _googleSchemaHotel,
                localTrip: false
            },
        ]
    };
    return global_merge_vars;
}

async function parseConfirmationFlightHotel(_params, _email) {
    var logObj = {
        function: "parseConfirmationFlightHotel"
        ,params: _params
        ,success: true
        ,level: 'info'
    };

    var global_merge_vars = await parseGlobalMergeVars(_params);

    let templateConfirmationScript = fs.readFileSync('views/templates/hotel.handlebars', 'utf8');
    //let templateVoucherHotelScript = fs.readFileSync('views/templates/voucher.handlebars', 'utf8');
    let templateVoucherHotelScript = fs.readFileSync('views/templates/hotel.handlebars', 'utf8');

    handlebarsService.addHelpers(handlebars);

    var templateVoucherHotelHtml = handlebars.compile("{{#content}}" + templateVoucherHotelScript + "{{/content}}");
    var compiledVoucherHotelHtml = templateVoucherHotelHtml(global_merge_vars);

    var templateConfirmationHtml = handlebars.compile("{{#content}}" + templateConfirmationScript + "{{/content}}", { noEscape: true }, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        }
    });

    var compiledConfirmationHtml = templateConfirmationHtml(global_merge_vars, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        };
    });

    //// Generar pdf
    var pdfHotel = await pdfGeneratorService.generatePdfFromStringToBuffer(compiledVoucherHotelHtml);

    var attachmentsPDF = [{
        type: "application/pdf",
        name: "VoucherHotel.pdf",
        content: pdfHotel
    }];

    var config = {
        html: compiledConfirmationHtml,
        subject: "Gracias por tu compra en Cocha.com",
        from_email: "agenteinternet@cocha.com",
        from_name: "Cocha",
        signing_domain: "cocha.com",
        to: [{
                email: _email || _params.paymentPassenger.email,
                name: _params.paymentPassenger.name + ' ' + _params.paymentPassenger.surname,
                type: "to"
        }],
        important: true,
        inline_css: true,
        attachments: attachmentsPDF,
    };
    config.to = addAdditionalBccEmails(config.to);
    return config;
};

/**
 * Adds more email addresses to the "to:" emails array.
 * @param {Array} _toArr
 * @returns {Array}
 */
function addAdditionalBccEmails(_toArr) {
    if (ENV === 'production') {
        const emailAddresses = [
            //BORRAR
            /*
            'agenteinternet@cocha.com',
            'cquezada@cocha.com',
            'jmpaz@cocha.com',
            'fgargiulo@cocha.com',
            'pramirez@cocha.com',
            'pvega@cocha.com',
            'cleon@cocha.com',
            'nmardones@cocha.com',
            */
            'ricardo.ovando@cocha.com'            
        ];
        emailAddresses.forEach((address) => {
            _toArr.push({
                email: address,
                type: 'bcc'
            });
        });
    }
    return _toArr;
}

async function parseConfirmationFlightHotelWithIds(_params, _email, _lastname) {
    var logObj = {
        function: "parseConfirmationFlightHotelWithIdHotel"
        , params: _params
        , success: true
        , level: 'info'
    };

    var global_merge_vars = await parseGlobalMergeVars(_params);

    let templateConfirmationScript = fs.readFileSync('views/templates/confirmationWithIds.handlebars', 'utf8');
    let templateVoucherHotelScript = fs.readFileSync('views/templates/voucher.handlebars', 'utf8');

    handlebarsService.addHelpers(handlebars);

    var templateVoucherHotelHtml = handlebars.compile("{{#content}}" + templateVoucherHotelScript + "{{/content}}");
    var compiledVoucherHotelHtml = templateVoucherHotelHtml(global_merge_vars);

    var templateConfirmationHtml = handlebars.compile("{{#content}}" + templateConfirmationScript + "{{/content}}", { noEscape: true }, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        }
    });

    var compiledConfirmationHtml = templateConfirmationHtml(global_merge_vars, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        };
    });

    //// Generar pdf

    var pdfHotel = await pdfGeneratorService.generatePdfFromStringToBuffer(compiledVoucherHotelHtml);

    var attachmentsPDF = [{
        type: "application/pdf",
        name: "Voucher de hotel.pdf",
        content: pdfHotel
    }];

    var config = {
        html: compiledConfirmationHtml,
        subject: "Gracias por tu compra en Cocha.com",
        from_email: "agenteinternet@cocha.com",
        from_name: "Cocha",
        signing_domain: "cocha.com",
        to: [{
                email: _email || _params.paymentPassenger.email,
                name: _params.paymentPassenger.name + ' ' + _params.paymentPassenger.surname,
                type: "to"
        }],
        important: true,
        inline_css: true,
        attachments: attachmentsPDF,
    };
    config.to = addAdditionalBccEmails(config.to);
    return config;

};

function parseGoogleSchemaHotel(_params) {
    var hotelSchema = {
        "@context": "http://schema.org",
        "@type": "",
        "reservationNumber": _params.numReserva,
        "reservationStatus": "http://schema.org/Confirmed",
        "underName": {
            "@type": "Person",
            "name": _params.paymentPassenger.name + ' ' + _params.paymentPassenger.surname
        },
        "reservationFor": {
            "@type": "LodgingBusiness",
            "name": _params.paymentPassenger.numConfirmacion,
            "address": {
                "@type": "PostalAddress",
                "streetAddress": _params.hotelDetailItinDTO.address,
                "addressLocality": _params.hotelDetailItinDTO.city || '',
                "addressRegion": _params.hotelDetailItinDTO.region || '',
                "postalCode": _params.hotelDetailItinDTO.zipCode,
                "addressCountry": _params.hotelDetailItinDTO.country
            },
            "telephone": _params.hotelPhone || ''
        },
        "checkinDate": _params.arrDate,
        "checkoutDate": _params.depDate
    };

    return hotelSchema;
}

async function sendAlertEmailNotSent(_params, _msg) {

    var buffTemp = Buffer.from(JSON.stringify(_params, null, 2));

    if (_.has(_params, "numReserva")) {
        var hotelItineraryInfo = {
            checkin: moment(_params.arrDate).format("dddd DD MMMM"),
            checkOut: moment(_params.depDate).format("dddd DD MMMM"),
            adultos: _params.paymentPassenger.adultsNum,
            niños: _params.paymentPassenger.childNum,
            noches: _params.numNights,
        }
    }

    var config = {
        "html": "<h1 style=\"color:red\">Voucher no enviado: *|pnr|*</h1><h2>No se ha envíado el voucher de confirmación de compra V+H al siguiente destinatario:</h2><ul><li>*|emailTo|*</li></ul><h2>El motivo podría resultar el siguiente:</h2><p> <b>*|errMsg|*</b></p><h3>Detalles</h3><h4>Contacto</h4><p>*|contactInfo|*</p><h4>Itinerario de vuelo</h4><p>*|flightItineraryInfo|*</p><h4>Itinerario del hotel</h4><p>*|hotelItineraryInfo|*</p><h4>Detalle del Hotel</h4><p>*|hotelDetailInfo|*</p>",
        "subject": "Error al enviar voucher",
        "from_email": "agenteinternet@cocha.com",
        "from_name": "Cocha.com",
        "to": [{ 
            //"email": "jmpaz@cocha.com",
            "email": "ricardo.ovando@cocha.com",
            "name": "Cocha.com",
            "type": "to"
        },{
            "email": "ricardo.ovando@cocha.com",
            "name": "Cocha.com",
            "type": "bcc"
        }],
        "important": true,
        "merge": true,
        "merge_language": "mailchimp",
        "global_merge_vars": [{
            "name": "pnr",
            "content": _params.codInterno
        }, {
            "name": "emailTo",
            "content": _params.paymentPassenger.email
        }, {
            "name": "contactInfo",
            "content": JSON.stringify(_params.paymentPassenger, null, 2) || "Sin Datos"
        },{
            "name": "hotelItineraryInfo",
            "content": JSON.stringify(_params.hotelDetailItinDTO, null, 2) || "Sin Datos"
        }, {
            "name": "errMsg",
            "content": _msg
        }, {
            "name": "errLog",
            "content": JSON.stringify(_params)
        }]
        , "attachments": [{
            "type": "text/plain; charset=UTF-8",
            "name": "log.txt",
            "content": buffTemp.toString('base64')
        }]
    };

    return await new Promise((resolve, reject) => {
        sendMailService.sendMail(config, (err, result) => {
            if (err) {
                Koa.log.error(err);
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}



module.exports = {
    parseConfirmationFlightHotel: parseConfirmationFlightHotel,
    parsePdfVoucher: parsePdfVoucher,
    parseConfirmationFlightHotelWithIds: parseConfirmationFlightHotelWithIds
}
