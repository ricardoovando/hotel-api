/**
 * UtilService.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

function addPaxAndRooms(_params, _flightParam, _hotelParam) {
	_flightParam.numADT = 0;
	_flightParam.numCHD = 0;
	_flightParam.numINF = 0;

	let room, pax;
	for (let numRoom = 1; numRoom < 4; numRoom++) {
		room = _params['room' + numRoom];					
		if (room) {
			_hotelParam['room' + numRoom] = room;
			room = room.split(',');
			_flightParam.numADT += Number(room[0]);
			for (let numPax = 1; numPax < room.length; numPax++) {
				pax = Number(room[numPax]);
				if (pax < 2) {
					_flightParam.numINF++;
				} else 
				if (pax < 12) {
					_flightParam.numCHD++;
				} else {
					_flightParam.numADT++;
				}
			}
		}
	}
}

function getIp(_ip){  
	if(!_ip){
		return '';
	}

	if(_ip.indexOf(',')>-1){
		return _ip.substr(0, _ip.indexOf(',')); 
	} else {
		return _ip;
	}
}

module.exports = {
	addPaxAndRooms: addPaxAndRooms
   ,getIp:getIp
};